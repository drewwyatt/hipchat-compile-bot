'use strict';
import {SwiftService} from './swift.service';

export class Parser {
  javascript(code: string): Promise<string> {
    return Promise.resolve(eval(code));
  }
  
  swift(code: string): Promise<string> {
    const service = new SwiftService();
    return service.post({ code });
  }
}