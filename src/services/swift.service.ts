'use strict';
import {HttpService, HttpOptions} from './http.service';

export class SwiftService extends HttpService {
  constructor() {
    super();
    this._options = {
      headers: {
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "cache-control": "no-cache"  
      },
      hostname: "swiftstub.com",
      method: "POST",
      path: "/run/",
      port: null,
      protocol: "http:"
    };
  }
}