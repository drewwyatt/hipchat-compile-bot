'use strict';
import {DeferredPromise} from '../helpers';
import * as http from 'http';
import * as fs from 'fs';
import * as qs from 'querystring';

export interface HttpOptions {
  headers: any;
  hostname: string;
  method: string;
  path: string;
  port: number;
  protocol: string;
}

export abstract class HttpService {
  protected _options: HttpOptions;

  post(data: any): Promise<string> {
    const deferred = new DeferredPromise();
    const req = http.request(this._options, (res) => {
      const chunks = [];
      
      console.log(res.statusCode);

      res.on("data", function(chunk) {
        chunks.push(chunk);
      });

      res.on("end", function() {
        const body = Buffer.concat(chunks);
        // console.log(body.toString());
        deferred.resolve(body.toString());
      });
    });

    req.write(qs.stringify(data));
    req.end();
    
    return deferred.promise;
  }
}
