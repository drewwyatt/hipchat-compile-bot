'use strict';

import {DeferredPromise} from '../helpers';
import {HipChat} from '../interfaces';
import {Language} from '../constants';
import {Parser} from './parser';

export class Message {
  constructor(private _messageObject: HipChat.Message) {}
  
  get body(): string {
    if(!this._body) {
      this._body = this._getBody();
    }
    
    return this._body
  }
  
  get language(): Language {
    if(!this._language) {
      this._language = this._getLanguage();
    } 
    
    return this._language;
  }
  
  parse(): Promise<string> {
    const deferred = new DeferredPromise();
    const parse = new Parser();
    
    try {
      switch(this.language) {
        case Language.JAVASCRIPT:
          deferred.resolve(parse.javascript(this.body));
          break;
          
        case Language.SWIFT:
          deferred.resolve(parse.swift(this.body));
          break;
          
        default:
          deferred.reject(new Error('That language is unupported.'));
          break;
      }
    } catch(err) {
      deferred.reject(new Error(`Parser error: ${err.message}`));
    }
    
    return deferred.promise;
  }
  
  get _message(): string {
    return this._messageObject.message;
  }
  
  private _body: string;
  private _language: Language;
  
  private _getBody(): string {
    let messageArr = this._message.split(' ');
    console.log('Removing...', messageArr.splice(0, 2));
    
    return messageArr.join(' ');
  }
  
  private _getLanguage(): Language {
    try {
      let lang: Language;
      
      switch(this._message.split(' ')[1].toLowerCase()) {
        case 'js':
          lang = Language.JAVASCRIPT;
          break;
        case 'swift':
          lang = Language.SWIFT;
          break;
        default:
          lang = Language.UNSUPPORTED;
          break;
      }
      
      return lang;
    } catch(err) {
      return Language.UNSUPPORTED;
    }
  }
}