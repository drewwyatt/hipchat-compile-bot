export module HipChat {
  export interface Request {
    event: string;
    item: Item;
    webhook_id: number;
  }
  
  export interface Item {
    message: Message;
    room: Room;
  }

  export interface Message {
    date: string;
    from: From;
    id: string;
    mentions: any[];
    message: string;
    type: string;
  }

  export interface From {
    id: number;
    mention_name: string;
    name: string;
  }
  
  export interface Room {
    id: number;
    name: string;
  }
}