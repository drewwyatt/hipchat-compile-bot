'use strict';
import {HipChat} from '../interfaces';
import {Message} from '../services';

export class Routes {
  static index(request, response): void {
    response.send('HipChat Compile Bot')
  }
  
  static compile(request, response): void {
    const hcRequest: HipChat.Request = request.body;
    const message = new Message(hcRequest.item.message);
    
    message.parse()
      .then(val => {
        response.send({
          "color": "green",
          "message": val,
          "notify": false,
          "message_format": "text"
        });
      })
      .catch(error => {
        response.send({
          "color": "red",
          "message": error.message,
          "notify": false,
          "message_format": "text"
        });
      });
  }
}