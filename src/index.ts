/// <reference path='../typings/tsd.d.ts' />
'use strict';

import * as express from 'express';
import * as bodyParser from 'body-parser';
const app = express();

import {Routes} from './routes';

app.set('port', (process.env.PORT || 5000));
app.use(bodyParser.json());

app.get('/', Routes.index);
app.post('/compile', Routes.compile);

app.listen(app.get('port'), () => {
  console.log("Node app is running at localhost:" + app.get('port'))
});
