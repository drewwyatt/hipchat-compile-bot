var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json', { sortOutput: true });

gulp.task('default', ['typescript']);

gulp.task('serve', function () {
  nodemon({
    script: './bin/src/index.js',
    ext: 'js',
    env: { 'NODE_ENV': 'development' }
  });
});

gulp.task('start', ['typescript', 'serve', 'watch']);

gulp.task('typescript', function () {
  var tsResult = tsProject.src()
    .pipe(ts(tsProject));

  return tsResult.js
    .pipe(gulp.dest('./bin'));
});

gulp.task('watch', function () {
  gulp.watch('./src/**/*.ts', ['typescript']);
});